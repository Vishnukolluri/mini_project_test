﻿Feature: LoginValidation



Scenario Outline: Post the Login Details
	Given UserName and Password
		| UserName   | Password   |
		| <Username> | <Password> |
	When We Post the UserName and Password
	Then StatusCode should be Created
	And Response body should contains UserName '<Username>'
Examples:
	| Username | Password     |
	| KolVishnu  | Biscuit#8763 |

Scenario Outline: Login on the Website
	Given URL of the Website
	When I Enter UserName '<UserName>' and '<Password>'
	And Click on the Login button
	Then UserName '<UserName>' should be on the dashboard
Examples:
	| UserName      | Password     |
	| VishnuKolluri | Biscuit#8763 |
