using NUnit.Framework;
using RestBusinessLayer.Responses;
using RestBusinessLayer.Utils;
using System;
using System.Security.Policy;
using TechTalk.SpecFlow;
using Utility.Utils;
using WebPages.Pages;

namespace TestFramework.StepDefinitions
{
    [Binding]
    public class BooksStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        private BooksValidResponse _booksResponse;
        private BookStorePage _bookStorePage;
        private List<Book> books;
        public BooksStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _bookStorePage = new BookStorePage(_scenarioContext);
        }
        [Given(@"Resource path of the Website")]
        public void GivenResourcePathOfTheWebsite()
        {
            _scenarioContext.Add("BooksUrl", ConfigReader.GetConfigValue("restUrl") + "BookStore/v1/Books");

        }

        [When(@"Get the Books")]
        public void WhenGetTheBooks()
        {
            string url = _scenarioContext.Get<string>("BooksUrl");
            _booksResponse=BooksUtils.GetBooks(url);
            books = _booksResponse.Books;
        }
        [Then(@"Open the Url of the Page")]
        public void ThenOpenTheUrlOfThePage()
        {
            _bookStorePage.OpenBookStoreUrl(ConfigReader.GetConfigValue("restUrl") + "books");
        }

        [Then(@"verify Title, Author, Publisher")]
        public void ThenVerifyTitleAuthorPublisher()
        {
            string author;
           for(int i = 1; i <= books.Count; i++)
            {
                author = books.ElementAt(i-1).Author;
                (string Title, string Author, string Publisher) = _bookStorePage.GetXpathsOfBooks(author, i);
                Assert.AreEqual(Title, books.ElementAt(i - 1).Title,"Book Title is not Matching");
                Assert.AreEqual(Author, books.ElementAt(i - 1).Author,"Book Author is not Matching");
                Assert.AreEqual(Publisher, books.ElementAt(i - 1).Publisher,"Book Publisher is not Matching");
            }
        }
    }
}
