using NUnit.Framework;
using RestBusinessLayer.Responses;
using RestBusinessLayer.Utils;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Utility.Utils;
using WebPages.Pages;

namespace TestFramework.StepDefinitions
{
    [Binding]
    public class LoginValidationStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        private static LoginValidResponse _loginValidResponse;
        private LoginPage _loginPage;
        private ProfilePage _profilePage;
        public LoginValidationStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _loginPage = new LoginPage(_scenarioContext);
            _profilePage = new ProfilePage(_scenarioContext);
        }
        [Given(@"UserName and Password")]
        public void GivenUserNameAndPassword(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            _scenarioContext.Add("UserName", data.UserName);
            _scenarioContext.Add("Password", data.Password);
        }

        [When(@"We Post the UserName and Password")]
        public void WhenWePostTheUserNameAndPassword()
        {
            string username = _scenarioContext.Get<string>("UserName");
            string password = _scenarioContext.Get<string>("Password");
            _loginValidResponse=LoginUtils.PostLoginDetails(username, password);
        }
        [Then(@"StatusCode should be Created")]
        public void ThenStatusCodeShouldBeCreated()
        {
            Assert.IsTrue(LoginUtils.VerifyLoginStatusCode(),"StatusCode is not Matching or not Created");
        }
        [Then(@"Response body should contains UserName '([^']*)'")]
        public void ThenResponseBodyShouldContainsUserName(string username)
        {
            Assert.That(_loginValidResponse.Username, Is.EqualTo(username),"UserName is not Matching");
        }
        //Website
        [Given(@"URL of the Website")]
        public void GivenURLOfTheWebsite()
        {
            _scenarioContext.Add("LoginUrl", ConfigReader.GetConfigValue("restUrl") + "login");
        }



        [When(@"I Enter UserName '([^']*)' and '([^']*)'")]
        public void WhenIEnterUserNameAnd(string userName, string password)
        {
            string url = _scenarioContext.Get<string>("LoginUrl");
            _loginPage.OpenLoginUrl(url);
            _loginPage.EnterUsername(userName);
            _loginPage.EnterPassword(password);
        }

        [When(@"Click on the Login button")]
        public void WhenClickOnTheLoginButton()
        {
            _loginPage.ClickLoginButton();
        }

        [Then(@"UserName '([^']*)' should be on the dashboard")]
        public void ThenUserNameShouldBeOnTheDashboard(string userName)
        {
            Assert.AreEqual(_profilePage.GetUserName(), userName,"UserName is not Matching");
        }

    }
}
