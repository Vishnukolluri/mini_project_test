﻿using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using TechTalk.SpecFlow;
using WebPages.Drivers;
using OpenQA.Selenium;

namespace TestFramework.Hooks
{
    [Binding]
    public sealed class Hooks1
    {
        private static AventStack.ExtentReports.ExtentReports s_extent;
        private static ExtentTest s_feature;
        private ExtentTest _scenario;
        private ExtentTest _step;
        private Driver _driverObj;
        private IWebDriver _driver;

        private readonly ScenarioContext _scenarioContext;
        private static string filePath = @"C:\\Users\\Vishnu_Kolluri\\source\\repos\\Utility\\Reports\\Reports";
        public Hooks1(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(filePath);
            s_extent = new AventStack.ExtentReports.ExtentReports();
            s_extent.AttachReporter(htmlReporter);
        }
        [BeforeFeature]
        public static void BeforeFeature(FeatureContext featureContext)
        {
            s_feature = s_extent.CreateTest(featureContext.FeatureInfo.Title);
        }
        [BeforeScenario]
        public void BeforeScenario()
        {
            Console.WriteLine("Before Scenario");
            if (_driverObj == null)
            {
                _driverObj = new Driver(_scenarioContext);
            }
            _scenarioContext.Add("Driver", _driverObj);
            _driver = _scenarioContext.Get<Driver>("Driver").SetUp();
            _scenarioContext.Add("BrowserDriver", _driver);
            _scenario = s_feature.CreateNode(_scenarioContext.ScenarioInfo.Title);
        }
        [BeforeStep]
        public void BeforeStep()
        {
            _step = _scenario;
        }
        [AfterScenario]
        public void AfterScenario()
        {
            Console.WriteLine("Quitting driver");
            _scenarioContext.Get<Driver>("Driver").Quit();
        }
        [AfterStep]
        public void AfterStep()
        {
            if (_scenarioContext.TestError == null)
            {
                _step.Log(Status.Pass, _scenarioContext.StepContext.StepInfo.Text);
            }
            else
            {
                _step.Log(Status.Fail, _scenarioContext.StepContext.StepInfo.Text);
                Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
                screenshot.SaveAsFile("ExceptionImage", ScreenshotImageFormat.Png);
            }

        }
        [AfterFeature]
        public static void AfterFeature()
        {
            s_extent.Flush();
        }
    }
}