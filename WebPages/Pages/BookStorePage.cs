﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace WebPages.Pages
{
    public partial class BookStorePage
    {
        private readonly ScenarioContext _scenarioContext;
        private IWebDriver _driver;
        public BookStorePage(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _driver = _scenarioContext.Get<IWebDriver>("BrowserDriver");
        }
        public (string title, string author, string publisher) GetXpathsOfBooks(string Author, int i)
        {
            string author = null;
            string bookAuthor=null;
            string bookPublisher=null;
            string bookTitle = null;
            if (Author.Contains("Richard"))
            {
                author = "Richard E. Silverman";
            }
            else if (Author.Contains("Addy"))
            {
                author = "Addy Osmani";
            }
            else if (Author.Contains("Glenn"))
            {
                author = "Glenn Block et al.";
            }
            else if (Author.Contains("Axel"))
            {
                author = "Axel Rauschmayer";
            }
            else if (Author.Contains("Eric"))
            {
                author = "Eric Elliott";
            }
            else if (Author.Contains("Marijn"))
            {
                author = "Marijn Haverbeke";
            }
            else if (Author.Contains("Nicholas"))
            {
                author = "Nicholas C. Zakas";
            }
            else if (Author.Contains("Kyle"))
            {
                author = "Kyle Simpson";
            }
            if (i % 2 != 0)
            {
                
                bookTitle = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]//ancestor::div[@class='rt-tr -odd']//a")).Text;
                bookAuthor = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]")).Text;
                bookPublisher = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]//following-sibling::div")).Text;
            }
            else if(i%2==0)
            {
                bookTitle = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]//ancestor::div[@class='rt-tr -even']//a")).Text;
                bookAuthor = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]")).Text;
                bookPublisher = _driver.FindElement(By.XPath($"//div[contains(text(),'{author}')]//following-sibling::div")).Text;
            }

            return (bookTitle, bookAuthor, bookPublisher);
        }


    }
}
