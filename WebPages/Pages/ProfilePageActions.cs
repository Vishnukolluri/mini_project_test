﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPages.Pages
{
    public partial class ProfilePage
    {
        public void ApplyWait(int seconds)
        {
            _driver.Manage().Timeouts().ImplicitWait=TimeSpan.FromSeconds(seconds);
        }
        public string GetUserName()
        {
            ApplyWait(10);
            return _driver.FindElement(_userNameValue).Text;
        }
    }
}
