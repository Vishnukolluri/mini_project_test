﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace WebPages.Pages
{
    public partial class ProfilePage
    {
        private readonly ScenarioContext _scenarioContext;
        private IWebDriver _driver;
        public ProfilePage(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _driver = _scenarioContext.Get<IWebDriver>("BrowserDriver");
        }
        private readonly By _userNameValue = By.Id("userName-value");

    }
}
