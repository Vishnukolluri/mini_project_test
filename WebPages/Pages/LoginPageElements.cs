﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace WebPages.Pages
{
    public partial class LoginPage
    {
        private readonly ScenarioContext _scenarioContext;
        private IWebDriver _driver;
        public LoginPage(ScenarioContext scenarioContext) 
        {
            _scenarioContext = scenarioContext;
            _driver=_scenarioContext.Get<IWebDriver>("BrowserDriver");
        }
        

        private readonly By _userName = By.Id("userName");
        private readonly By _password = By.Id("password");
        private readonly By _loginButton = By.Id("login");
        
    }
}
