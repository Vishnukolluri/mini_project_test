﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Utils;

namespace WebPages.Pages
{
    public partial class LoginPage
    {
        public void OpenLoginUrl(string url)
        {
            _driver.Navigate().GoToUrl(url);
            _driver.Manage().Window.Maximize();
        }
        public void EnterUsername(string username)
        {
            _driver.FindElement(_userName).SendKeys(username);
        }
        public void EnterPassword(string password)
        {
            _driver.FindElement(_password).SendKeys(password);
        }
        public void ClickLoginButton()
        {
            
            WebDriverWait wait = new WebDriverWait(_driver,TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementToBeClickable(_loginButton));
            IWebElement element=_driver.FindElement(_loginButton);
            IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
            js.ExecuteScript("arguments[0].scrollIntoView()", element);
            element.Click();
        }

    }
}
