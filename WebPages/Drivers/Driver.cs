﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Utility.Utils;

namespace WebPages.Drivers
{
    public class Driver
    {
        private IWebDriver _driver;
        private readonly ScenarioContext _scenarioContext;
        public Driver(ScenarioContext scenarioContext)
        {
            this._scenarioContext = scenarioContext;
        }
        public IWebDriver SetUp()
        {
            var browserName = ConfigReader.GetConfigValue("browserName");

            _driver = GetBrowserDriver(browserName);

            _scenarioContext.Add("WebDriver", _driver);
            return _driver;
        }
        public IWebDriver GetBrowserDriver(string browserName)
        {
            if (browserName == "FireFox")
            {
                _driver = new FirefoxDriver();
            }
            else if (browserName == "Edge")
            {
                _driver = new EdgeDriver();
            }
            else
            {
                _driver = new ChromeDriver();
            }
            return _driver;
        }
       
        public void Quit()
        {
            _scenarioContext.Get<IWebDriver>("WebDriver").Quit();
        }
    }
}
