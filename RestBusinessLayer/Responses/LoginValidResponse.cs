﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestBusinessLayer.Responses
{
    public class LoginValidResponse
    {
        [JsonProperty("userID")]
        public string UserID { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("books")]
        public List<object> Books { get; set; }
    }
}
