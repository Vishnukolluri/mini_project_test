﻿using Newtonsoft.Json;
using RestBusinessLayer.Requests;
using RestBusinessLayer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.RestUtils;

namespace RestBusinessLayer.Utils
{
    public class LoginUtils
    {
        public static LoginValidResponse PostLoginDetails(string username, string password)
        {
            return RestUtility.Post<LoginValidResponse>("Account/v1/User",CreateRequestBody(username,password),RestSharp.DataFormat.Json);
        }

        private static string CreateRequestBody(string username, string password)
        {
            LoginValidRequest request = new LoginValidRequest();
            request.UserName = username;
            request.Password = password;
            return JsonConvert.SerializeObject(request);
        }
        public static bool VerifyLoginStatusCode()
        {
            return RestUtility.VerifyLoginStatusCode();
        }
        
    }
}
