﻿using RestBusinessLayer.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.RestUtils;

namespace RestBusinessLayer.Utils
{
    public class BooksUtils
    {
        public static BooksValidResponse GetBooks(string resource)
        {
            return RestUtility.Get<BooksValidResponse>(resource,RestSharp.DataFormat.Json);
        }
    }

}
