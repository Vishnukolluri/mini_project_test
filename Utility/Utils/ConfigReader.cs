﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Utils
{
    public static class ConfigReader
    {
        public static IConfiguration? Configuration { get; set; }
        public static IConfiguration Read()
        {
            var Configuration=new ConfigurationBuilder()
                .AddJsonFile("C:\\Users\\Vishnu_Kolluri\\source\\repos\\TestFramework\\BooksConfig.json").Build();
            return Configuration;
        }

        public static string GetConfigValue(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException(nameof(key));
            }
            if(Configuration==null)
            {
                Configuration=Read();
            }
            return Configuration[key];
        }
    }
}
