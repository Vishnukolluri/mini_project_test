﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Utils
{
    public class LogHelper
    {
        private static ILog s_logger;
        private static ConsoleAppender s_consoleAppender;
        private static FileAppender s_fileAppender;
        private static RollingFileAppender s_rollingFileAppender;
        private static string s_layout = "%date{dd:MM:yyyy} [%level] [%class] [%method] - %message%newline";
        private string _layout
        {
            set { s_layout = value; }
        }
        private static PatternLayout GetPatternLayout()
        {
            var patternLayout = new PatternLayout()
            {
                ConversionPattern = s_layout

            };
            patternLayout.ActivateOptions();
            return patternLayout;
        }
        private static ConsoleAppender GetConsoleAppender()
        {
            var consoleAppender = new ConsoleAppender()
            {
                Name = "ConsoleAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All
            };
            consoleAppender.ActivateOptions();
            return consoleAppender;
        }
        private static FileAppender GetFileAppender()
        {
            var fileAppender = new FileAppender()
            {
                Name = "fileAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                AppendToFile = true,
                File = @"C:\\Users\\Vishnu_Kolluri\\source\\repos\\Utility\\Loggers\\FileLog.log"
            };
            fileAppender.ActivateOptions();
            return fileAppender;
        }
        private static RollingFileAppender GetRollingFileAppender()
        {
            var rollingFileAppender = new RollingFileAppender()
            {
                Name = "rollingfileAppender",
                Layout = GetPatternLayout(),
                Threshold = Level.All,
                AppendToFile = true,
                File = @"C:\\Users\\Vishnu_Kolluri\\source\\repos\\Utility\\Loggers\\RollingFileLog.log",
                MaximumFileSize = "1KB",
                MaxSizeRollBackups = 12
            };
            rollingFileAppender.ActivateOptions();
            return rollingFileAppender;
        }
        public static ILog GetLogger(Type type)
        {
            if (s_consoleAppender == null)
            {
                s_consoleAppender = GetConsoleAppender();
            }
            if (s_fileAppender == null)
            {
                s_fileAppender = GetFileAppender();
            }
            if (s_rollingFileAppender == null)
            {
                s_rollingFileAppender = GetRollingFileAppender();
            }
            if (s_logger != null)
            {
                return s_logger;
            }
            BasicConfigurator.Configure(s_consoleAppender, s_fileAppender, s_rollingFileAppender);
            s_logger = LogManager.GetLogger(type);
            return s_logger;
        }
    }
}
