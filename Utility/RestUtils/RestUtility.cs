﻿using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utility.Utils;

namespace Utility.RestUtils
{
    public class RestUtility
    {
        private static ILog s_logger=LogHelper.GetLogger(typeof(RestUtility));
        
        private static RestClient _restClient;
        private static RestRequest _restRequest;
        private static RestResponse _restResponse;
        public static RestClient RestClient
        {
            get
            {
                if (_restClient == null)
                {
                    return new RestClient(ConfigReader.GetConfigValue("restUrl"));
                }
                else
                {
                    return _restClient;
                }
            }
        }
        public static RestRequest CreateRequest(string resource, Method method, DataFormat dataFormat)
        {
            if (_restRequest == null)
            {
                var restRequest = new RestRequest(resource, method);
                restRequest.RequestFormat = dataFormat;
                return restRequest;
            }
            else
            {
                return _restRequest;
            }
        }
        public static T Post<T>(string resource,string payLoad,DataFormat dataFormat)
        {
            _restResponse=RestClient.Execute(CreateRequest(resource,Method.Post,dataFormat).AddBody(payLoad));
            string content = _restResponse.Content;
            return JsonConvert.DeserializeObject<T>(content);
        }
        public static bool VerifyLoginStatusCode()
        {
            s_logger.Debug(_restResponse.StatusCode);
            return _restResponse.StatusCode.Equals(HttpStatusCode.Created);
        }
        public static T Get<T>(string resource, DataFormat dataFormat) 
        {
            var content = RestClient.Execute(CreateRequest(resource, Method.Get, dataFormat)).Content;
            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
